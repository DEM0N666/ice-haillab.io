import { shallowMount } from '@vue/test-utils'
import ActionButton from '@/components/form/ActionButton.vue'
import sinon from 'sinon'

describe('ActionButton.vue', () => {
  it('renders elements', () => {
  
    const wrapper = shallowMount(ActionButton, {
      propsData: { 
        text: "Button Text",
        action: () => {}
      }
    });
    expect(wrapper.text()).toMatch("Button Text");
  });

  it('calls callback on click', () => {
    
    const cb = sinon.fake();
    const wrapper = shallowMount(ActionButton, {
      propsData: { 
        text: "Button Text",
        action: cb
      }
    });

    expect(cb.notCalled).toEqual(true);
    wrapper.findAll("button").trigger("click");
    expect(cb.calledOnce).toEqual(true);
  });
});
