import Vue from 'vue'
import store from './store'
import VueAWN from "vue-awesome-notifications"

import PageTitle from './pages/PageTitle';
import PageEditor from './pages/PageEditor';

require("vue-awesome-notifications/dist/styles/style.css");

Vue.use(VueAWN, {
  icons: {enabled: false},
  duration: 5000
});

Vue.config.productionTip = false
window.app = null;

window.addEventListener("load", () => {
  window.app = new Vue({
    store,
    data: {
      currentRoute: sessionStorage.getItem("route") || "title"
    },
    render: function (h) { return h(this.ViewComponent) },
    computed: {
      ViewComponent()
      {
        sessionStorage.setItem("route", this.currentRoute);
        return this.currentRoute == "editor" ? PageEditor : PageTitle;
      }
    }
  }).$mount('#app');
});
