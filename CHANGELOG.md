## Changelog

### v0.3.0
- Spheres are now supported
- Added "climbable" flag for entries

### v0.2.0
- Cleaner UI
- Import of a reference model (CSV) + texture
- Change opacity of the reference model
- File-type filter added to file uploads
- Fixed wrong Z-rotation