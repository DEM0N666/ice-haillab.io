import { shallowMount } from '@vue/test-utils'
import InputVector3 from '@/components/form/InputVector3.vue'

describe('InputVector3.vue', () => {
  it('renders elements', () => {
  
    const wrapper = shallowMount(InputVector3, {
      propsData: { vector: [1,2,3] }
    });
    const inputs = wrapper.findAll("input");

    expect(wrapper.text()).toMatch("x y z");
    expect(inputs.length).toEqual(3);
  });

  it('input changes vector, values are numbers', () => {
  
    const vector = [1,2,3];
    const wrapper = shallowMount(InputVector3, {
      propsData: { vector }
    });
    const inputs = wrapper.findAll("input");

    inputs.at(0).setValue("1.2");
    inputs.at(1).setValue("2.2");
    inputs.at(2).setValue("3.2");

    expect(vector[0]).toEqual(1.2);
    expect(vector[1]).toEqual(2.2);
    expect(vector[2]).toEqual(3.2);
  });
});
