/**
 * @copyright 2019 - Max Bebök
 * @author Max Bebök
 * @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
 */

 /**
  * This file format is horrible, and i can't await the heat death of the universe
  * that will destroy this abomination
  */
export class CsvLoader
{
    constructor()
    {
        this.name = "-NoName-";
        this.vertexModelArray = [];
        this.indexModelArray = [];
        this.normalArray = [];
        this.uvArray = [];

        this.vertexArray = [];
        this.indexArray = [];
    }

    load(data)
    {
        const lines = data.split("\n");
        let readMode = "";

        for (let i=0; i<lines.length; ++i) 
        {
            const line = lines[i].replace(/\r/g, '').trim();
            
            if(line.startsWith("Obj Name")) {
                this.name = line.substr("Obj Name:".length);
                continue;
            }

            if(line.startsWith("vert_Array")) {
                readMode = "vertex";
                continue;
            }
            if(line.startsWith("face_Array")) {
                readMode = "index";
                continue;
            }

            if(line.endsWith("_Array")) {
                readMode = "unknown";
                continue;
            }

            switch(readMode)
            {
                case "vertex":
                    const vertex = this._lineToVector(line);
                    this.vertexArray.push(vertex);
                    this.vertexModelArray.push(...vertex);
                    i += 1;
                    this.normalArray.push(...this._lineToVector(lines[i]));
                    i += 2;
                    this.uvArray.push(...this._lineToVector(lines[i]));
                break;

                case "index":
                    const indices = this._lineToVector(line, -1);
                    this.indexArray.push(indices);
                    this.indexModelArray.push(...indices);
                break;
            }
        }
    }

    createModel()
    {
        console.log(`Create model ${this.name}`);
        const mat = new THREE.MeshLambertMaterial({transparent: true});

        const geometry = new THREE.BufferGeometry();
        geometry.setIndex(this.indexModelArray);
        geometry.addAttribute('position', new THREE.BufferAttribute(new Float32Array(this.vertexModelArray), 3));
        geometry.addAttribute('normal', new THREE.BufferAttribute(new Float32Array(this.normalArray), 3));
        geometry.addAttribute('uv', new THREE.BufferAttribute(new Float32Array(this.uvArray), 2));
        
        geometry.computeVertexNormals();

        return new THREE.Mesh(geometry, mat);  
    }

    getVertices()
    {
        const polygons = [];
        let posIndex = 0;
        while(posIndex < this.indexModelArray.length)
        {
            let index0 = this.indexModelArray[posIndex++] * 3;
            let index1 = this.indexModelArray[posIndex++] * 3;
            let index2 = this.indexModelArray[posIndex++] * 3;

            polygons.push([
                this.vertexModelArray[index0], this.vertexModelArray[index0 + 1], this.vertexModelArray[index0 + 2],
                this.vertexModelArray[index1], this.vertexModelArray[index1 + 1], this.vertexModelArray[index1 + 2],
                this.vertexModelArray[index2], this.vertexModelArray[index2 + 1], this.vertexModelArray[index2 + 2],
                //this.vertexArray[index2], this.vertexArray[index2 + 1], this.vertexArray[index2 + 2], // same to make a "quad"
                this.vertexModelArray[index0], this.vertexModelArray[index0 + 1], this.vertexModelArray[index0 + 2],
            ]);
        }

        return polygons;
    }

    _lineToVector(line, offset = 0)
    {
        return line.split(",").map(val => parseFloat(val) + offset);
    }
}

 export default function loadCSV(data)
 {
    const loader = new CsvLoader();
    loader.load(data);
    return loader.createModel();
 }